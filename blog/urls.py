from django.urls import path
from .views import (
    PostListView,
    PostDetailView,
    PostCreateView,
    PostUpdateView,
    PostDeleteView,
    UserPostListView,
    AddLike,
    AddDislike,
    AddCommentView,
    ApproveCommentView,
)
from . import views

urlpatterns = [
    path('', PostListView.as_view(), name='blog-home'),
    path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    path('post/new/', PostCreateView.as_view(), name='post-create'),
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
    path('about/', views.about, name='blog-about'),
    path('user/<str:username>', UserPostListView.as_view(), name='user-posts'),
    path('post/<int:pk>/like/', AddLike.as_view(), name='like_post'),
    path('post/<int:pk>/dislike/', AddDislike.as_view(), name='dislike_post'),
    path('comment/<int:pk>/approve/', ApproveCommentView.as_view(), name='comment_approve'),
    path('post/<int:pk>/comment/', AddCommentView.as_view(), name='add_comment'),
    path('book_of_the_day', views.book_of_the_day, name='book_of_the_day'),
    path('novi-zapysy/', views.zapisy, name='post_list'),
    path('kuplu-prodam/', views.booklist, name='book_list'),
    path('obhovorennya/', views.discussions, name='discussion_list'),
    path('literaturni-branchi/', views.branch, name='event_list'),
]
