import requests


def get_book_info():
    url = 'https://openlibrary.org/api/books?bibkeys=random&format=json&jscmd=data'
#    params = {'api_key': 'МІЙ АПІ КЛЮЧ - НАЧЕ У САЙТУ Є ТІЛЬКИ АПІ ДЛЯ КОЖНОЇ КНИГИ'}
    response = requests.get(url, params='params')
    data = response.json()
    book_key = list(data.keys())[0]
    book_info = data[book_key]
    book_title = book_info.get('title', '')
    book_author = book_info.get('authors', [{}])[0].get('name', '')
    book_cover_url = book_info.get('cover', {}).get('medium', '')
    book_read_url = book_info.get('url', '')
    book_download_url = book_info.get('formats', {}).get('epub', {}).get('download_link', '')
    return {
        'title': book_title,
        'author': book_author,
        'cover_url': book_cover_url,
        'read_url': book_read_url,
        'download_url': book_download_url,
    }
