from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.utils import timezone
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.views import View
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import User
from datetime import datetime
from .forms import CommentForm
from .models import Post, Comment
from .openlibrary import get_book_info


def home(request):
    context = {
        'posts': Post.objects.all()
    }
    return render(request, 'blog/home.html', context)


class PostListView(ListView):
    model = Post
    template_name = 'blog/home.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']
    paginate_by = 5


class PostDetailView(DetailView):
    model = Post
    template_name = 'blog/post_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CommentForm()
        context['comments'] = self.object.comments.filter(approved_comment=True)
        return context


class AddCommentView(LoginRequiredMixin, CreateView):
    model = Comment
    form_class = CommentForm

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.post_id = self.kwargs['pk']
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('post-detail', kwargs={'pk': self.kwargs['pk']})


class ApproveCommentView(LoginRequiredMixin, UpdateView):
    model = Comment
    fields = ('approved_comment',)

    def form_valid(self, form):
        form.instance.approved_comment = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('post-detail', kwargs={'pk': self.object.post.pk})


class AddLike(LoginRequiredMixin, View):
    def post(self, request, pk):
        post = Post.objects.get(pk=pk)

        is_dislike = False

        for dislike in post.dislikes.all():
            if dislike == request.user:
                is_dislike = True
                break

        if is_dislike:
            post.dislikes.remove(request.user)

        is_like = False

        for like in post.likes.all():
            if like == request.user:
                is_like = True
                break

        if not is_like:
            post.likes.add(request.user)

        if is_like:
            post.likes.remove(request.user)

        return HttpResponseRedirect(reverse('post-detail', args=[str(pk)]))


class AddDislike(LoginRequiredMixin, View):
    def post(self, request, pk):
        post = Post.objects.get(pk=pk)

        is_like = False

        for like in post.likes.all():
            if like == request.user:
                is_like = True
                break

        if is_like:
            post.likes.remove(request.user)

        is_dislike = False

        for dislike in post.dislikes.all():
            if dislike == request.user:
                is_dislike = True
                break

        if not is_dislike:
            post.dislikes.add(request.user)

        if is_dislike:
            post.dislikes.remove(request.user)

        return HttpResponseRedirect(reverse('post-detail', args=[str(pk)]))


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content', 'image']

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.image = self.request.FILES.get('image')
        return super().form_valid(form)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content', 'image']

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.image = self.request.FILES.get('image')
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


def about(request):
    return render(request, 'blog/about.html', {'title': 'Про блог "Що почитати...'})


def zapisy(request):
    today = timezone.now().date()
    posts = Post.objects.filter(date_posted__date=today)

    if not posts:
        message = "Сьогодні немає нових записів. Приходьте завтра!"
        return render(request, 'blog/zapisy.html', {'message': message})

    return render(request, 'blog/zapisy.html', {'posts': posts})


def booklist(request):
    return render(request, 'blog/booklist.html', {'title': 'Купити чи продати книгу в Україні'})


def discussions(request):
    return render(request, 'blog/discussions.html', {'title': 'Говоримо про книги'})


def branch(request):
    return render(request, 'blog/branch.html', {'title': 'Наші недільні літературні бранчі'})


class UserPostListView(ListView):
    model = Post
    template_name = 'blog/user_posts.html'
    context_object_name = 'posts'
    paginate_by = 5

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('-date_posted')


def book_of_the_day(request):
    book_info = get_book_info()
    book = {
        'cover': book_info['cover']['medium'],
        'title': book_info['title'],
        'author': book_info['authors'][0]['name'],
        'read_url': f"https://openlibrary.org{book_info['identifiers']['openlibrary'][0]['url']}",
        'download_url': f"https://openlibrary.org{book_info['formats']['ebook'][0]['url']}"
    }
    context = {'book': book}
    return render(request, 'blog/book_of_the_day.html', context)
