from django import forms
from .models import Post, Comment


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'image', 'content', 'date_posted', 'author']
        widgets = {
            'image': forms.ImageField(),
        }

    def save(self, commit=True):
        post = super().save(commit=False)
        post.image = self.cleaned_data.get('image')

        if commit:
            post.save()

        return post


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text', 'created_date')
        widgets = {
            'body': forms.Textarea(attrs={'cols': 180}),
        }


class Meta:
    permissions = [('can_upload_image', 'Can upload image')]
